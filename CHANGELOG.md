[0.0.1]
* Initial version
* Use EspoCRM to 4.8.4

[0.1.0]
* Update EspoCRM to 5.0.5

[0.2.0]
* Update EspoCRM to 5.1.0

[0.3.0]
* Fix apache config

[0.4.0]
* Run cronjob on startup to fix warning display

[0.4.1]
* Update logo

[0.5.0]
* Disable new version notification

[0.5.1]
* Protect data directory in apache configs

[0.5.2]
* Fix issue where was not starting after a restart

[1.0.0]
* Update EspoCRM to 5.2.5
* [Announcement](https://www.espocrm.com/blog/espocrm-5-2-0-released/)
* Data Privacy: Ability to view personal data;
* Data Privacy: Ability to erase personal data;
* Data Privacy: Parameter to mark new email addresses opted out by default;
* Shared Calendar;
* Kanban view;
* Icons in the vertical navbar;
* Specific colors for entity types (for a quicker recognition);
* Entity Manager: Ability to pick an icon for an entity type;
* Entity Manager: Ability to reset to defaults;
* Calendar: Displaying users avatars on the timeline view;
* Opportunities: Convert Currency list view action.

[1.1.0]
* Update EspoCRM to 5.3.2
* [Announcement](https://www.espocrm.com/blog/espocrm-5-3-0-released/)
* Full-text search;
* Mail Merge;
* Layout Manager: Dynamic logic for panels visibility;
* Layout Manager: Ability to remove default side panel or move it below other panels;
* Supporting link-multiple fields on list views;
* Mass Print to PDF action from list view;
* Print to PDF: Ability to print link-multiple fields;
* Text Filter: Supporting Auto-increment and Integer fields;
* Opportunity: Last Stage field; Ability to show sales pipeline based on the last stage field;
* Formula: countRelated, sumRelated functions.
* Date/DateTime fields: ‘Use Numeric Format’ parameter.

[1.1.1]
* Update EspoCRM to 5.3.4

[1.1.2]
* Update EspoCRM to 5.3.5

[1.1.3]
* Update EspoCRM to 5.3.6

[1.2.0]
* Update EspoCRM to 5.4.1

[1.2.1]
* Update EspoCRM to 5.4.2

[1.2.2]
* Update EspoCRM to 5.4.3

[1.3.0]
* Use latest base image

[1.3.1]
* Update EspoCRM to 5.4.4

[1.3.2]
* Update EspoCRM to 5.4.5

[1.4.0]
* Update EspoCRM to 5.5.1
* Ability to search in related record list;
* Activities: Ability to search in Activities and History of a specific record;
* Stream: Ability to search posts in record's stream;
* Template manager (customizing email messages sent by application);
* Enum field: Ability to specify color style for options;
* Ability to define autocomplete country list;
* Varchar field: Ability to define autocomplete suggestions;
* Email Templates: Today and Now placeholders;
* Timeline: Displaying busy timeframes in shared view;

[1.4.1]
* Update EspoCRM to 5.5.3

[1.4.2]
* Update EspoCRM to 5.5.4

[1.4.3]
* Update EspoCRM to 5.5.5

[1.4.4]
* Update EspoCRM to 5.5.6

[1.5.0]
* Update EspoCRM to 5.6.0
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/5.6.0)
* WebSocket support;
* Ability to restore deleted records for admin;
* 'Recalculate Formula' mass action for admin;
* Ability to mass unlink related records;
* Ability to view & manage a list of followers of a specific record;
* Meetings: All-day meetings;
* Meetings/Calls: Acceptance button;
* Meetings/Calls: 'Set Held' and 'Set Not Held' actions on quick view modal;
* Phone Numbers: Invalid and Opted-Out attributes;
* Emails: Ability to view/manage user list, email record is available for;

[1.5.1]
* Update EspoCRM to 5.6.1
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/5.6.1)
* WebSocket: Not working event popup notifications

[1.5.2]
* Update EspoCRM to 5.6.2
* Import: Multiple email addresses support;
* Foreign field: Email and Phone field types support;
* Lead Capture: Ability to view payload data in log;
* Lead Capture: Ability to disable duplicate check.

[1.6.0]
* Update manifest to v2

[1.6.1]
* Update EspoCRM to 5.6.3

[1.6.2]
* Update EspoCRM to 5.6.4

[1.6.3]
* Update EspoCRM to 5.6.5
* Fix XSS issue

[1.6.4]
* Update EspoCRM to 5.6.6

[1.6.5]
* Update EspoCRM to 5.6.7

[1.6.6]
* Update EspoCRM to 5.6.8

[1.6.7]
* Update EspoCRM to 5.6.9

[1.6.8]
* Make module installation work

[1.7.0]
* Update EspoCRM to 5.6.11

[1.7.1]
* Update EspoCRM to 5.6.12

[1.7.2]
* Update EspoCRM to 5.6.13

[1.7.3]
* Update EspoCRM to 5.6.14

[1.8.0]
* Update EspoCRM to 5.7.0

[1.8.1]
* Update EspoCRM to 5.7.1

[1.8.2]
* Update EspoCRM to 5.7.2

[1.8.3]
* Update EspoCRM to 5.7.3

[1.8.4]
* Update EspoCRM to 5.7.4

[1.8.5]
* Update EspoCRM to 5.7.5

[1.8.6]
* Update EspoCRM to 5.7.6

[1.8.7]
* Update EspoCRM to 5.7.7

[1.8.8]
* Update EspoCRM to 5.7.8

[1.8.9]
* Update EspoCRM to 5.7.9

[1.8.10]
* Update EspoCRM to 5.7.10

[1.8.11]
* Update EspoCRM to 5.7.11

[1.8.12]
* Preserve aclStrictMode

[1.9.0]
* Update EspoCRM to 5.8.0

[1.9.1]
* Update EspoCRM to 5.8.1

[1.9.2]
* Update EspoCRM to 5.8.2

[1.9.3]
* Update EspoCRM to 5.8.3

[1.9.4]
* Update EspoCRM to 5.8.4

[1.10.0]
* Add support for voip integration

[1.11.0]
* Enable web socket support

[1.11.1]
* Update EspoCRM to 5.8.5

[1.12.0]
* Use latest base image 2.0.0

[1.13.0]
* Update EspoCRM to 5.9.0
* [Full changes](https://github.com/espocrm/espocrm/releases/tag/5.9.0)

[1.13.1]
* Update EspoCRM to 5.9.1
* [Full changes](https://github.com/espocrm/espocrm/releases/tag/5.9.1)
* Mass Email: Not sending if VERP is enabled bug

[1.13.2]
* Update EspoCRM to 5.9.2
* [Full changes](https://github.com/espocrm/espocrm/releases/tag/5.9.2)
* Roles: Edit view improvements #1707
* Scheduled jobs: Showing human readable description of the scheduling #1712
* Scheduled jobs: Validation for scheduling expression #1713
* Relationship panel: 'Create' button on 'Select' dialog #1724
* Password recovery: Parameter to prevent email address exposure on recovery form #1728
* Password recovery: Parameter to disable password recovery for all internal users #1729

[1.13.3]
* Update EspoCRM to 5.9.3
* [Full changes](https://github.com/espocrm/espocrm/releases/tag/5.9.3)
* Vertical navbar not scrollable in some cases bug
* Select followers button is not working bug

[1.13.4]
* Improve apache handling

[1.13.5]
* Update EspoCRM to 5.9.4
* [Full changes](https://github.com/espocrm/espocrm/releases/tag/5.9.4)
* Formula: Numbers with zero decimal part are parsed as integers bug
* Markdown: Links inside code blocks displayed incorrectly bug
* Date field may display values with 1-day shift in some cases bug
* Formula: Result of dayOfWeek function depends on language set in settings bug
* Formula: record\count & record\exists do not support usage of a same where key multiple times bug
* Formula: Using quote characters within comments breaks the script bug

[1.14.0]
* Update EspoCRM to 6.0.0
* [Full changes](https://github.com/espocrm/espocrm/releases/tag/6.0.0)
* Import: Ability to run import with parameters of a previously run import #1736
* Import: Ability to resume failed import from the last processed row #1735
* Import: Ability to run from CLI #1734
* Emails: Ability to insert record field values when compose #1730
* Admin panel: Quick search #1756
* Console: Command printing all available container services #1748
* Formula: Language translate functions #1783
* Formula: json\retrieve function #1796
* Image field: 'Preview size in list view' parameter #1799
* Campaign Log: Search by email address and queue item ID #1811
* Emails: Hiding modal window dialog after Send button is clicked #1737
* Mass Email: Teams of campaign record are copied to all sent emails #1780
* Email Accounts: TLS support for IMAP #1791
* Admin panel: Quick search #1756

[1.14.1]
* Update EspoCRM to 6.0.1
* Label Manager: Save does not work #1813

[1.14.2]
* Update EspoCRM to 6.0.2
* [Full changes](https://github.com/espocrm/espocrm/releases/tag/6.0.2)

[1.14.3]
* Update EspoCRM to 6.0.3
* [Full changes](https://github.com/espocrm/espocrm/milestone/102?closed=1)

[1.14.4]
* Update EspoCRM to 6.0.4
* Added currency format '10 €' #1830

[1.14.5]
* Update EspoCRM to 6.0.5
* Fixed: Duplicating record with attachment-multiple fields causing attachment being detached from their field
* Fixed: Searching by person name is not working when format is Last First Middle
* Fixed: Export is not available for admin if disabled in settings

[1.14.6]
* Update EspoCRM to 6.0.6
* Fixed: XLSX Export error when there are rows with empty currency

[1.14.7]
* Update EspoCRM to 6.0.7
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/6.0.7)

[1.14.8]
* Update EspoCRM to 6.0.8
* Category tree paths being created improperly
* Global auto-follow can't be saved bug
* Notifications about posts don't work for portal users bug

[1.14.9]
* Update EspoCRM to 6.0.9
* [Full changelog](https://github.com/espocrm/espocrm/milestone/108?closed=1)

[1.14.10]
* Update EspoCRM to 6.0.10
* [Full changelog](https://github.com/espocrm/espocrm/milestone/110?closed=1)

[1.15.0]
* Update EspoCRM to 6.1.0
* [Release Announcement](https://forum.espocrm.com/forum/announcements/66733-espocrm-6-1-0-released)
* Navbar: Tab groups #1848
* Log: Ability to specify which handlers to use #1861
* Kanban: Ability to sort items within a group #1825
* Array field: Ability to add multiple items at once #1874
* Entity Manager: Separate page for entity #1833
* List view: Showing 'Apply' button when filter added, removed or changed #1831

[1.16.0]
* [Support custom domains for portals](https://docs.cloudron.io/apps/espocrm/#portals)

[1.16.1]
* Update EspoCRM to 6.1.1
* [Full changelog](https://github.com/espocrm/espocrm/milestone/111?closed=1)

[1.16.2]
* Update EspoCRM to 6.1.2
* Fix Kanban ordering

[1.17.0]
* Use base image v3
* Update PHP to 7.4
* cron is now run every minute

[1.17.1]
* Add a log for cron jobs

[1.17.2]
* Update EspoCRM to 6.1.3
* Save & Continue Editing action for PDF templates, email templates and knowledge base #1919
* Formula: array\join function #1917
* Formula: util\generateId function #1912

[1.17.3]
* Update EspoCRM to 6.1.4
* Printing to PDF not working
* Attachment size not properly calculated

[1.17.4]
* Update EspoCRM to 6.1.5

[1.17.5]
* Update EspoCRM to 6.1.6

[1.17.6]
* Set `REMOTE_ADDR` to the real IP address

[1.17.7]
* Update EspoCRM to 6.1.7

[1.17.8]
* Update EspoCRM to 6.1.8

[1.17.9]
* Update EspoCRM to 6.1.9

[1.17.10]
* Update EspoCRM to 6.1.10

[2.0.0]
* Update EspoCRM to 7.0.8
* [Blog announcement](https://www.espocrm.com/blog/version-7-0/)
* breaking changes: `aclStrictMode` is gone. please see the [forum post](https://forum.cloudron.io/topic/5911/espocrm-7)
* Optimistic concurrency control
* Meetings from invitation emails
* AWS S3 storage support
* 2 factor authentication enhancements
* Layout manager: Ability to specify conditions on which a color will be applied to a panel

[2.1.0]
* Optional SSO support

[2.1.1]
* Update base image to 3.2.0

[2.1.2]
* Update EspoCRM to 7.0.9

[2.1.3]
* Update EspoCRM to 7.0.10
* Admin don't have access to email folders of another user bug
* Kanban header not sliding with cards container when more records shown (show more) bug
* Forward email not copying attachments bug
* Follow button shows incorrectly after edit is cancelled bug
* Entity Manager: Empty 'Ignored groups in Kanban view' not saved bug 

[2.2.0]
* Update EspoCRM to 7.1.0
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.1.0)

[2.2.1]
* Update EspoCRM to 7.1.1
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.1.1)

[2.2.2]
* Update EspoCRM to 7.1.2

[2.2.3]
* Update EspoCRM to 7.1.3
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.1.3)
* Error after sending invitations bug
* Entity Manager: Status options not available in 'Ignored groups in Kanban view' bug
* Global search not ordering results correctly (with full-text) bug 

[2.2.4]
* Update EspoCRM to 7.1.4
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.1.4)

[2.2.5]
* Update EspoCRM to 7.1.5
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.1.5)
* Converting lead causing error if duplicate exists bug
* All-day event duration is changed with 1 day added after date-start is changed bug
* Contains-search not applied for autocomplete bug 

[2.2.6]
* Update EspoCRM to 7.1.6
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.1.6)
* Range-float field not working

[2.2.7]
* Update EspoCRM to 7.1.7
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.1.7)
* Wysiwyg: Pasting image uploads twice

[2.2.8]
* Update EspoCRM to 7.1.8
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.1.8)
* Import preview XSS security
* When sending email from case - email is sent to secondary contact and CC primary contact bug
* Clicking Save button in label manager, without any changes made, causes error 500 bug
* Checking ACL read action on user model error bug
* User w/o delete access can't mass-convert currency bug

[2.2.9]
* Update EspoCRM to 7.1.9
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.1.9)

[2.2.10]
* Update EspoCRM to 7.1.10
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.1.10)
*  Post not rendered when posting to an empty stream #2378 

[2.2.11]
* Update EspoCRM to 7.1.11
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.1.11)
* Idle mass action modal not updating when finished

[2.3.0]
* Update EspoCRM to 7.2.0
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.2.0)
* Detail view tabs https://github.com/espocrm/espocrm/issues/2379
* Bottom panel tabs https://github.com/espocrm/espocrm/issues/2384
* Mass Update: Ability to add/remove items https://github.com/espocrm/espocrm/issues/2310
* Import: Errors panel https://github.com/espocrm/espocrm/issues/2372
* Email: Custom fields support https://github.com/espocrm/espocrm/issues/2366
* Separate view for stream note records https://github.com/espocrm/espocrm/issues/2398
* Varchar/Multi-enum/Array fields: Pattern parameter to check a value against https://github.com/espocrm/espocrm/issues/2360
* Command renaming custom entity type https://github.com/espocrm/espocrm/issues/2318

[2.3.1]
* Update EspoCRM to 7.2.1
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.2.1)
* Not working if webserver stripping custom headers bug
* Values in link (URL) fields are encoded improperly bug

[2.3.2]
* Update EspoCRM to 7.2.2
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.2.2)
* Language can't be changed in settings bug
* Attachment-multiple: Not clickable in edit mode bug

[2.3.3]
* Update EspoCRM to 7.2.3
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.2.3)
* Uploading file in chunks causing type validation error in same cases bug
* File type validation not working with upper case extensions bug
* Layout manager: Bottom panels tab-breaks not being saved in same cases bug
* Inline images not working in system emails bug

[2.3.4]
* Update EspoCRM to 7.2.4
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.2.4)
* Email account: Can't save monitored folders other than Inbox bug
* Calendar: Showing now-indicator on agenda view improvement
* Printing validation error details in log back-end
* Lead capture: Not-working if the teams field is required bug
* Lead: Industry options are not available if no access to Account scope bug 

[2.3.5]
* Update EspoCRM to 7.2.5
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.2.5)
* **IMPORTANT:** This release broke LDAP login, update to package v2.3.6 immediately

[2.3.6]
* Update EspoCRM to 7.2.6
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.2.6)
* This release fixes the LDAP login issue

[2.4.0]
* Update EspoCRM to 7.2.7
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.2.7)
* Allow email from address display name to set

[2.5.0]
* Update base image to 4.0.0

[2.6.0]
* Update EspoCRM to 7.3.0
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.3.0)
* Working Calendar #2415
* Logging in as another user for admin #2444
* OIDC authentication #2455
* Email: Group folders #2467
* Separate list view for related records #2485
* Meeting/Call: Ability to send cancellation emails for not-held events #2518
* Selectbox improvements #2508
* Checkbox visual improvement #2507
* Reminder: Allowing to specify arbitrary values #2511
* Email: Mass Move-to-Folder action #2441
* Email filter: Mark as read #2479
* Email: Drag and dropping into folders #2466
* Email: Shortcut keys #2489
* Email: Corresponding from-address pre-selected when replying on email #2522
* Meeting/Call: Users automatically follow events they are participating in #2512
* Meeting/Call: Ability to select attendees when sending invitation email #2517
* Meeting/Call: Stream note about attendance confirmation #2505
* Meeting/Call: Scheduler on modal edit view #2521
* Number field: Console command to populate numbers for existing records #232
* Login-handler framework #2446
* Meeting/Call: In-app notifications for users added as attendees #2492
* Record service: Ability to specify what access is required when linking records (in metadata) #2503
* Sync login/logout between browser tabs #2506
* Email address/Phone number: Is-invalid boolean field #2520
* Formula: Ability to obtain emailAddressData, phoneNumberData from entity #2528
* Currency no-join mode #2535

[2.6.1]
* Update EspoCRM to 7.3.1
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.3.1)

[2.6.2]
* Update EspoCRM to 7.3.2
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.3.2)

[2.6.3]
* Update EspoCRM to 7.3.3
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.3.3)

[2.6.4]
* Update EspoCRM to 7.3.4
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.3.4)

[2.7.0]
* Update EspoCRM to 7.4.0
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.4.0)
* PDF: Dompdf engine #2564
* OIDC: Portal support #2578
* PostgreSQL experimental support #2605
* Hard database rebuild #2592
* Formula: if-then-else & while statements #2536
* Formula: break & continue in while-loops #2551
* Formula: record\delete function Formula: record\delete function
* Formula: string\matchExtract function #2651
* Formula: ext\currency\convert function #2659
* MariaDB performance improvements #2601
* Currency field: Support Decimal DB type #2547
* Int/Float/Currency fields: Inline calculation #2552
* XLSX export: Less memory consuming mode #2534
* XLSX export: Parameters enabling printing title & links to records #2533
* Email: View attachments in modal #2553
* Separate parameter to configure number of records initially displayed in Kanban columns #2526

[2.7.1]
* Update EspoCRM to 7.4.1
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.4.1)
* 2FA support in portals.

[2.7.2]
* Update EspoCRM to 7.4.2
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.4.2)
* Currency field: Issue when decimal places value is empty #2692 

[2.7.3]
* Update EspoCRM to 7.4.3
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.4.3)
* Issue with int/float/currency fields when thousand separator is . #2693 

[2.7.4]
* Update EspoCRM to 7.4.4
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.4.4)

[2.7.5]
* Update EspoCRM to 7.4.5
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.4.5)

[2.7.6]
* Update EspoCRM to 7.4.6
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.4.6)

[2.8.0]
* Update EspoCRM to 7.5.0
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.5.0)
* API Before-Save script #2720
* Enum options re-use #2695
* URL-Multiple field type #2713
* Copy to clipboard button #2698
* Memo dashlet #2699
* Emails: Print action #2706
* Email filters: 'Body contains all' filter #2708
* Quick search when adding a field filter #2709
* Text filter: Previous search suggestions #2710

[2.8.1]
* Update EspoCRM to 7.5.1
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.5.1)
*  Email notifications not working with Assigned Users present #2768

[2.8.2]
* Update EspoCRM to 7.5.2
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.5.2)

[2.8.3]
* Update EspoCRM to 7.5.3
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.5.3)

[2.8.4]
* Update EspoCRM to 7.5.4
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.5.4)

[2.8.5]
* Update EspoCRM to 7.5.5
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.5.5)
* Wrong default encoding in PDO connection
* Fetching all followers of a records fetches duplicates instead of the real followers

[2.8.6]
* Update EspoCRM to 7.5.6
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/7.5.6)
* Jobs running in-parallel processes not working in PHP 8.1.22, 8.2.9 (exclusively) #2828

[2.9.0]
* Update EspoCRM to 8.0.0
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.0.0)
* Navbar tab grouping #2780
* Light theme #2799
* Export customizations to installable extension package #2794
* Duplicate checking fields #2790
* Layout sets for specific users #2796
* Entity Manager: Parameter 'Duplicate check on update' #2789
* Custom list layouts for relationship panels #2792
* My Inbox dashlet: Folder option #2760
* Roles: Message permission #2784
* Formula: record\findMany function #2785
* Not allowing to remove required link from foreign record #2797
* List (Small) layout on small screen #2800
* ES modules #2762
* ES classes #2766
* ORM: Ability to join sub-queries #2756
* CORS middleware #2755

[2.9.1]
* Update EspoCRM to 8.0.1
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.0.1)
* Fixed a few regression bugs in Calendar.

[2.9.2]
* Update EspoCRM to 8.0.2
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.0.2)
* Calendar: Not translated day labels on month view
* Email Template: br instead of line breaks for text fields in plain-text mode

[2.9.3]
* Update EspoCRM to 8.0.3
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.0.3)

[2.9.4]
* Update EspoCRM to 8.0.4
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.0.4)

[2.9.5]
* Update EspoCRM to 8.0.5
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.0.5)

[2.9.6]
* Update EspoCRM to 8.0.6
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.0.6)

[2.10.0]
* Update EspoCRM to 8.1.0
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.1.0)
* AGPL license https://github.com/espocrm/espocrm/pull/2931
* Discard TCPDF engine https://github.com/espocrm/espocrm/issues/2910
* Forbidding link & unlink API operations for Has-One, Belongs-To & Belongs-To-Parent links https://github.com/espocrm/espocrm/issues/2882
* International phone numbers https://github.com/espocrm/espocrm/issues/2896
* Ability to hide/show columns on the list view https://github.com/espocrm/espocrm/issues/2891
* Dynamic Logic: Conditions checking current user & current user teams https://github.com/espocrm/espocrm/issues/2912
* Mass email: Parameter to limit max number of emails sent per batch https://github.com/espocrm/espocrm/issues/2930
* Read-only-After-Create field parameter https://github.com/espocrm/espocrm/issues/2843
* Link, Link-Multiple fields: Autocomplete on empty input https://github.com/espocrm/espocrm/issues/2903
* Link, Link-multiple fields: Create button https://github.com/espocrm/espocrm/issues/2894
* Phone field: Config parameter forcing numeric search https://github.com/espocrm/espocrm/issues/2895
* ORM: Ability to reference aliases https://github.com/espocrm/espocrm/issues/2844
* Row Actions framework https://github.com/espocrm/espocrm/issues/2886
* Handler method checking availability for main view menu items https://github.com/espocrm/espocrm/issues/2888
* Custom navbar items https://github.com/espocrm/espocrm/issues/2889
* Custom email template placeholders https://github.com/espocrm/espocrm/issues/2875
* PDF: Markdown text helper https://github.com/espocrm/espocrm/issues/2925

[2.10.1]
* Update EspoCRM to 8.1.1
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.1.1)

[2.11.0]
* Move to OIDC login

[2.11.1]
* Move to OIDC login
* Update EspoCRM to 8.1.2
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.1.2)

[2.11.2]
* Update EspoCRM to 8.1.3
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.1.3)

[2.11.3]
* Update EspoCRM to 8.1.4
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.1.4)

[2.11.4]
* Update EspoCRM to 8.1.5
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.1.5)
* Slow email loading issue fixed

[2.12.0]
* Update EspoCRM to 8.2.0
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.2.0)
* List view pagination https://github.com/espocrm/espocrm/issues/3018
* Audit Log https://github.com/espocrm/espocrm/issues/2979
* Global Stream https://github.com/espocrm/espocrm/issues/2944
* Stream: View user activity https://github.com/espocrm/espocrm/issues/2945
* Applying time zone for scheduled jobs https://github.com/espocrm/espocrm/issues/2936
* Ability to define custom order to apply when selecting related records https://github.com/espocrm/espocrm/issues/2941
* Filling email address when compose email from Base Plus entity https://github.com/espocrm/espocrm/issues/2942
* PDF: Ability to define CSS in Templates https://github.com/espocrm/espocrm/issues/2949
* Wysiwyg: Code view improvements https://github.com/espocrm/espocrm/issues/2951
* PDF: Iteration with attributehttps://github.com/espocrm/espocrm/issues/2952
* Config override https://github.com/espocrm/espocrm/issues/2916
* Import: Relate by foreign field https://github.com/espocrm/espocrm/issues/2956
* Ability to override API routes https://github.com/espocrm/espocrm/issues/2962
* Link-Parent field: Parameter to enable autocomplete on empty input https://github.com/espocrm/espocrm/issues/2963
* Roles: UX improvements https://github.com/espocrm/espocrm/issues/2968
* Calendar: Slot duration parameter in preferences https://github.com/espocrm/espocrm/issues/2971
* Emails: Record search when adding email addresses https://github.com/espocrm/espocrm/issues/2973
* Navbar tabs with custom URLs https://github.com/espocrm/espocrm/issues/2974
* Panel notes https://github.com/espocrm/espocrm/issues/2977
* Formula: entity\clearAttribute function https://github.com/espocrm/espocrm/issues/2980
* Adding prefix to entity type, field & link names https://github.com/espocrm/espocrm/issues/2988
* Primary filter in URI parameter in list view https://github.com/espocrm/espocrm/issues/2990
* Relationship Manager: Ability to specify select primary filter https://github.com/espocrm/espocrm/issues/3002
* Decreasing max length of City, Country & State fields https://github.com/espocrm/espocrm/issues/2986

[2.12.1]
* Update EspoCRM to 8.2.1
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.2.1)

[2.12.2]
* Update EspoCRM to 8.2.2
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.2.2)

[2.12.3]
* Update EspoCRM to 8.2.3
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.2.3)

[2.12.4]
* Update EspoCRM to 8.2.4
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.2.4)

[2.12.5]
* Update EspoCRM to 8.2.5
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.2.5)

[2.13.0]
* Update EspoCRM to 8.3.0
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.3.0)

[2.13.1]
* Update EspoCRM to 8.3.1
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.3.1)

[2.13.2]
* Update EspoCRM to 8.3.2
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.3.2)

[2.13.3]
* Update EspoCRM to 8.3.3
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.3.3)

[2.13.4]
* Update EspoCRM to 8.3.4
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.3.4)

[2.13.5]
* Update EspoCRM to 8.3.5
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.3.5)

[2.13.6]
* Update EspoCRM to 8.3.6
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.3.6)
* Can't edit email if from address is not own and no access to Import https://github.com/espocrm/espocrm/issues/3116

[2.14.0]
* Update EspoCRM to 8.4.0
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.4.0)
* Emails: Archive folder https://github.com/espocrm/espocrm/issues/3088
* Emails: Import EML https://github.com/espocrm/espocrm/issues/3082
* Emails: Hiding quote part https://github.com/espocrm/espocrm/issues/3086
* Kanban: Ability to show/hide fields https://github.com/espocrm/espocrm/issues/3079
* PDF Templates: Conditional rendering attribute x-if https://github.com/espocrm/espocrm/issues/3122
* PDF Templates: Logical and equal helpers https://github.com/espocrm/espocrm/issues/3121
* Lead: Ability to select existing Account when converting to Contact https://github.com/espocrm/espocrm/issues/3090
* Mass Email: One-click unsubscribe email headers https://github.com/espocrm/espocrm/issues/3094
* Meeting/Call: List attendees in ICS file https://github.com/espocrm/espocrm/issues/3089
* Navbar menu defined in metadata https://github.com/espocrm/espocrm/issues/3080
* Mass Email: Unsubscribe requires click https://github.com/espocrm/espocrm/issues/3094
* Methods actionCreateRelated and actionSelectRelated are moved out from views/detail https://github.com/espocrm/espocrm/issues/3111
* Fix midKeys order for many-to-many relationships https://github.com/espocrm/espocrm/issues/3108

[2.14.1]
* Update EspoCRM to 8.4.1
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.4.1)

[2.14.2]
* Update EspoCRM to 8.4.2
* [Full changelog](https://github.com/espocrm/espocrm/releases/tag/8.4.2)

[2.14.3]
* checklist added to CloudronManifest

[2.15.0]
* Update espocrm to 9.0.0
* [Full Changelog](https://github.com/espocrm/espocrm/releases/tag/9.0.0)
* Collaborators https://github.com/espocrm/espocrm/issues/3190
* Reactions https://github.com/espocrm/espocrm/issues/3194
* Emails: Scheduled Send https://github.com/espocrm/espocrm/issues/3200
* Web-to-Lead forms https://github.com/espocrm/espocrm/issues/3217
* View user access https://github.com/espocrm/espocrm/issues/3199
* App secrets https://github.com/espocrm/espocrm/issues/3148
* Tab quick search https://github.com/espocrm/espocrm/issues/3173
* Multiple assigned users enabled with parameter in entity manager https://github.com/espocrm/espocrm/issues/3158
* Stream Updated At field https://github.com/espocrm/espocrm/issues/3209
* Stream notes about assignment for entity types with multiple assigned users https://github.com/espocrm/espocrm/issues/3159
* Cases: Hidden from portal https://github.com/espocrm/espocrm/issues/3213
* Formula: Support object in record\create and record\update https://github.com/espocrm/espocrm/issues/3160
* Formula: ext\markdown\transform function https://github.com/espocrm/espocrm/issues/3169
* Ability to set links read-only via admin UI https://github.com/espocrm/espocrm/issues/3162
* Stream: Quote reply on posts https://github.com/espocrm/espocrm/issues/3163
* Emails: Notification about assignment https://github.com/espocrm/espocrm/issues/3171
* Emails: Ability to move emails from group folders to Archive and Trash https://github.com/espocrm/espocrm/issues/3172
* Emails: Adding email to inbox of assigned user when creating task https://github.com/espocrm/espocrm/issues/3177
* Emails in stream improvements https://github.com/espocrm/espocrm/issues/3237

[2.15.1]
* Update espocrm to 9.0.1
* [Full Changelog](https://github.com/espocrm/espocrm/releases/tag/9.0.1)

[2.15.2]
* Update espocrm to 9.0.2
* [Full Changelog](https://github.com/espocrm/espocrm/releases/tag/9.0.2)

[2.15.3]
* Update espocrm to 9.0.3
* [Full Changelog](https://github.com/espocrm/espocrm/releases/tag/9.0.3)

[2.15.4]
* Update espocrm to 9.0.4
* [Full Changelog](https://github.com/espocrm/espocrm/releases/tag/9.0.4)

