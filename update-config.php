<?php
include "bootstrap.php";

$fm = new \Espo\Core\Utils\File\Manager();

$db = parse_url(getenv('CLOUDRON_MYSQL_URL'));
$db_config = [
    'database' => [
        'driver' => 'pdo_mysql',
        'host' =>  $db['host'],
        'port' => '',
        'charset' => 'utf8mb4',
        'dbname' => substr($db['path'], 1),
        'user' => $db['user'],
        'password' =>  $db['pass']
    ],
    'isInstalled' => true,
];

$internal_config = require("/app/data/data/config-internal.php");
$internal_config = array_replace($internal_config, $db_config);
file_put_contents("/app/data/data/config-internal.php", $fm->wrapForDataExport($internal_config));

$cloudron_config = [
    'smtpServer' => getenv('CLOUDRON_MAIL_SMTP_SERVER'),
    'smtpPort' => getenv('CLOUDRON_MAIL_SMTP_PORT'),
    'smtpAuth' => true,
    'smtpSecurity' => '',
    'smtpUsername' => getenv('CLOUDRON_MAIL_SMTP_USERNAME'),
    'smtpPassword' => getenv('CLOUDRON_MAIL_SMTP_PASSWORD'),
    'outboundEmailFromAddress' => getenv('CLOUDRON_MAIL_FROM'),
    'outboundEmailFromName' => getenv('CLOUDRON_MAIL_FROM_DISPLAY_NAME') ?: 'EspoCRM',
    'adminNotificationsNewVersion' => false,
    'useWebSocket' => true,
    'siteUrl' => getenv('CLOUDRON_APP_ORIGIN'),
];

$oidc_config = [];
if (getenv('CLOUDRON_OIDC_ISSUER') !== false) {
    // CLOUDRON_OIDC_PROVIDER_NAME is not supported
    $oidc_config = [
        'authenticationMethod' => 'Oidc',
        'oidcClientId' => getenv('CLOUDRON_OIDC_CLIENT_ID'),
        'oidcClientSecret' => getenv('CLOUDRON_OIDC_CLIENT_SECRET'),
        'oidcAuthorizationEndpoint' => getenv('CLOUDRON_OIDC_AUTH_ENDPOINT'),
        'oidcTokenEndpoint' => getenv('CLOUDRON_OIDC_TOKEN_ENDPOINT'),
        'oidcJwksEndpoint' => getenv('CLOUDRON_OIDC_KEYS_ENDPOINT'),
        'oidcCreateUser' => true,
        'oidcSync' => true,
        'oidcAllowRegularUserFallback' => false,
        'oidcAllowAdminUser' => true,
        'authTokenPreventConcurrent' => false,
        'auth2FA' => false,
        'oidcJwtSignatureAlgorithmList' => [
            0 => 'RS256'
         ],
        'oidcUsernameClaim' => 'sub',
        'oidcFallback' => true,
        'passwordRecoveryDisabled' => true,
        'oidcTeamsIds' => [],
        'oidcTeamsNames' => (object) [],
        'oidcTeamsColumns' => (object) [],
        'oidcGroupClaim' => NULL,
        'oidcSyncTeams' => false,
        'oidcLogoutUrl' => NULL,
        'oidcScopes' => [
            0 => 'openid',
            1 => 'profile',
            2 => 'email'                                                                                ],
    ];
}

$config = require("/app/data/data/config.php");
$config = array_replace($config, $cloudron_config);
$config = array_replace($config, $oidc_config);

// get rid of ldap settings (can be removed on the next release)
$config = array_filter($config, fn($key) => !str_starts_with($key, 'ldap'), ARRAY_FILTER_USE_KEY);

file_put_contents("/app/data/data/config.php", $fm->wrapForDataExport($config));
