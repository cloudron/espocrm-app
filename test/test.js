#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    fs = require('fs'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
} 

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;

    let app;
    let browser;
    let contactId;
    let athenticated_by_oidc = false;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        let inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    async function login(username, password) {
        await browser.get('https://' + app.fqdn);

        await browser.wait(until.elementLocated(By.id('field-userName')), TIMEOUT);

        // click login Fallback btn (available if oidc active)
        if (await browser.findElements(By.xpath('//a[@role="button" and @data-action="showFallback"]')).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//a[@role="button" and @data-action="showFallback"]')).click();
        }

        await browser.findElement(By.id('field-userName')).sendKeys(username);
        await browser.findElement(By.id('field-password')).sendKeys(password);
        await browser.findElement(By.id('btn-login')).click();
        await browser.wait(until.elementLocated(By.xpath('//span[text()="My Activities"]')), TIMEOUT);
    }

    async function loginOIDC(username, password) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(2000);

        await browser.wait(until.elementLocated(By.xpath('//button[@id="sign-in" and contains(., "Sign in")]')), TIMEOUT);
        await browser.findElement(By.xpath('//button[@id="sign-in" and contains(., "Sign in")]')).click();
        await browser.sleep(2000);

        if (!athenticated_by_oidc) {
            const originalWindowHandle = await browser.getWindowHandle();
            const allWindowHandles = await browser.getAllWindowHandles();
            const newWindowHandle = allWindowHandles.filter((h) => h !== originalWindowHandle).join();

            await browser.switchTo().window(newWindowHandle);

            await waitForElement(By.xpath('//input[@name="username"]'));
            await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
            await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
            await browser.sleep(2000);
            await browser.findElement(By.id('loginSubmitButton')).click();
            await browser.sleep(2000);

            athenticated_by_oidc = true;

            await browser.switchTo().window(originalWindowHandle);
        }
        await browser.sleep(20000);
        await browser.wait(until.elementLocated(By.xpath('//span[text()="My Activities"]')), TIMEOUT);
    }

    async function adminLogin() {
        await login('admin', 'changeme');
    }

    async function createContact() {
        await browser.get('https://' + app.fqdn + '/#Account/create');

        await waitForElement(By.xpath('//input[@data-name="name"]'));
        await browser.findElement(By.xpath('//input[@data-name="name"]')).sendKeys('cloudroncontact');
        await browser.findElement(By.xpath('//button[text()="Save"]')).click();
        await browser.wait(function () {
            return browser.getCurrentUrl().then(function (url) {
                contactId = url.slice(url.lastIndexOf('/')+1);
                return url.startsWith('https://' + app.fqdn + '/#Account/view/');
            });
        }, TIMEOUT);

        console.log(`Contact ID is ${contactId}`);

        // give it time to commit
        await browser.sleep(2000);
    }

    async function contactExists() {
        await browser.get(`https://${app.fqdn}/#Account/view/${contactId}`);
        await waitForElement(By.xpath('//span[contains(text(), "cloudroncontact")]'));
    }

    async function contactExistsOld() {
        await browser.get(`https://${app.fqdn}/#Account/view/${contactId}`);
        await waitForElement(By.xpath('//div[contains(text(), "cloudroncontact")]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);

        await waitForElement(By.xpath('//a[@id="nav-menu-dropdown"]'));
        await browser.findElement(By.xpath('//a[@id="nav-menu-dropdown"]')).click();

        await waitForElement(By.xpath('//a[text()="Log Out"]'));
        await browser.findElement(By.xpath('//a[text()="Log Out"]')).click();

        await browser.sleep(5000);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app (no sso)', async function () {
        execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS);
        await browser.sleep(10000); // wait for sometime for the setup to finish
    });
    it('can get app information', getAppInfo);

    it('can admin login', adminLogin);
    it('can create contact', createContact);
    it('can check contact', contactExists);
    it('can logout', logout);

    it('uninstall app (no sso)', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    it('install app', async function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        await browser.sleep(10000); // wait for sometime for the setup to finish
    });

    it('can get app information', getAppInfo);

    it('can admin login', adminLogin);
    it('can create contact', createContact);
    it('can check contact', contactExists);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, USERNAME, PASSWORD));
    it('can logout', logout);

    it('can restart app', async function () {
        execSync('cloudron restart --app ' + app.id);
        await browser.sleep(10000);
    });

    it('can admin login', adminLogin);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, USERNAME, PASSWORD));
    it('can logout', logout);

    it('can backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(10000);
    });

    it('can admin login', adminLogin);
    it('can check contact', contactExists);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, USERNAME, PASSWORD));
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
    });

    it('can admin login', adminLogin);
    it('can check contact', contactExists);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, USERNAME, PASSWORD));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app for update', async function () {
        execSync('cloudron install --appstore-id com.espocrm.cloudronapp --location ' + LOCATION, EXEC_ARGS);
        await browser.sleep(10000); // wait for sometime for the setup to finish
    });
    it('can get app information', getAppInfo);
    it('can admin login', adminLogin);
    it('can create contact', createContact);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, USERNAME, PASSWORD));
    it('can logout', logout);

    it('can update', async function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
        await browser.sleep(10000);
    });

    it('can admin login', adminLogin);
    it('can check contact', contactExists);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, USERNAME, PASSWORD));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});

